import React,{Component} from 'react';

import TodoItem from './TodoItem';

class TodoList extends Component{
    constructor(props){
        super(props);
        
        this.state = {
            todoList: []
        };

        this.renderListItem = this.renderListItem.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let todoList = this.state.todoList;
        
        todoList.push(nextProps.task);
        this.setState({todoList});

    }

    deleteTask(task) {
        let delList = this.state.todoList;
        let check = delList.indexOf(task);
        delList.splice(check,1);
        this.setState({delList});
    }

    renderListItem(){
        return this.state.todoList.map((list,i)=>{
            return(
                <li key={i} >
                    <TodoItem deleteTask={(task)=>{this.deleteTask(task)}} task={list} />
                </li>
            )
        })
    }
    
    render(){
        return(
            <div>
                <ul>
                    {this.renderListItem()}
                </ul>
            </div>        
        )
    }
}

export default TodoList;