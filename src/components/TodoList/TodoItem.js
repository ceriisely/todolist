import React,{Component} from 'react';

class TodoItem extends Component{
    constructor(props){
        super(props);

        this.onClickDel = this.onClickDel.bind(this);

    }

    onClickDel(){
        this.props.deleteTask(this.props.task)
    }
    
    render(){
        return(
            <div>
                <div>{this.props.task}</div>
                <button type="button" className="btn btn-danger" onClick={this.onClickDel}>delete</button>
            </div>
        );
    }
}

export default TodoItem;