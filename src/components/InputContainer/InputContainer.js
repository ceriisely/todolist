import React,{Component} from 'react';

class InputContainer extends Component{

    constructor(props){
        super(props);

        this.onClickAddTask = this.onClickAddTask.bind(this);
    }

    onClickAddTask(){
        this.props.onTaskAdd(this.refs.addTaskInput.value);
    }

    render(){
        return(
            <div className="center InputCtn" >
                <input className="form-group form-control" ref="addTaskInput" placeholder="Add new task..."/>
                <button type="button" className="btn btn-success btn-add" onClick={this.onClickAddTask}>Add</button>
            </div>
        );
    }
}

export default InputContainer;